// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vc_fifo.h for the primary calling header

#include "Vc_fifo.h"
#include "Vc_fifo__Syms.h"

//==========

VL_CTOR_IMP(Vc_fifo) {
    Vc_fifo__Syms* __restrict vlSymsp = __VlSymsp = new Vc_fifo__Syms(this, name());
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vc_fifo::__Vconfigure(Vc_fifo__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
    Verilated::timeunit(-12);
    Verilated::timeprecision(-12);
}

Vc_fifo::~Vc_fifo() {
    VL_DO_CLEAR(delete __VlSymsp, __VlSymsp = NULL);
}

void Vc_fifo::_settle__TOP__2(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_settle__TOP__2\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->fifoEmpty = (((1U & ((IData)(vlTOPp->wr_ptr) 
                                 >> 4U)) == (1U & ((IData)(vlTOPp->rd_ptr) 
                                                   >> 4U))) 
                         & ((0xfU & (IData)(vlTOPp->wr_ptr)) 
                            == (0xfU & (IData)(vlTOPp->rd_ptr))));
    vlTOPp->fifoFull = (((1U & ((IData)(vlTOPp->wr_ptr) 
                                >> 4U)) != (1U & ((IData)(vlTOPp->rd_ptr) 
                                                  >> 4U))) 
                        & ((0xfU & (IData)(vlTOPp->wr_ptr)) 
                           == (0xfU & (IData)(vlTOPp->rd_ptr))));
    vlTOPp->dataOutput = vlTOPp->circular_fifo__DOT__registers
        [vlTOPp->rd_ptr];
}

void Vc_fifo::_eval_initial(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_eval_initial\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void Vc_fifo::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::final\n"); );
    // Variables
    Vc_fifo__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vc_fifo::_eval_settle(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_eval_settle\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__2(vlSymsp);
}

void Vc_fifo::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_ctor_var_reset\n"); );
    // Body
    clk = VL_RAND_RESET_I(1);
    reset = VL_RAND_RESET_I(1);
    push = VL_RAND_RESET_I(1);
    pop = VL_RAND_RESET_I(1);
    dataIn = VL_RAND_RESET_I(16);
    wr_ptr = VL_RAND_RESET_I(5);
    rd_ptr = VL_RAND_RESET_I(5);
    dataOutput = VL_RAND_RESET_I(16);
    fifoFull = VL_RAND_RESET_I(1);
    fifoEmpty = VL_RAND_RESET_I(1);
    { int __Vi0=0; for (; __Vi0<32; ++__Vi0) {
            circular_fifo__DOT__registers[__Vi0] = VL_RAND_RESET_I(16);
    }}
    { int __Vi0=0; for (; __Vi0<32; ++__Vi0) {
            circular_fifo__DOT__dmuxRegisters[__Vi0] = VL_RAND_RESET_I(1);
    }}
    circular_fifo__DOT__i = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<2; ++__Vi0) {
            __Vm_traceActivity[__Vi0] = VL_RAND_RESET_I(1);
    }}
}
