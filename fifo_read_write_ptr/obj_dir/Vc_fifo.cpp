// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vc_fifo.h for the primary calling header

#include "Vc_fifo.h"
#include "Vc_fifo__Syms.h"

//==========

void Vc_fifo::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vc_fifo::eval\n"); );
    Vc_fifo__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        vlSymsp->__Vm_activity = true;
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("c_fifo.sv", 1, "",
                "Verilated model didn't converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Vc_fifo::_eval_initial_loop(Vc_fifo__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        _eval_settle(vlSymsp);
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("c_fifo.sv", 1, "",
                "Verilated model didn't DC converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

VL_INLINE_OPT void Vc_fifo::_sequent__TOP__1(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_sequent__TOP__1\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v0;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v1;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v1;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v2;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v3;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v3;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v4;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v5;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v5;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v6;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v7;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v7;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v8;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v9;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v9;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v10;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v11;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v11;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v12;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v13;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v13;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v14;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v15;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v15;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v16;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v17;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v17;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v18;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v19;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v19;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v20;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v21;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v21;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v22;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v23;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v23;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v24;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v25;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v25;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v26;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v27;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v27;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v28;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v29;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v29;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v30;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v31;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v31;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v32;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v33;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v33;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v34;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v35;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v35;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v36;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v37;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v37;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v38;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v39;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v39;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v40;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v41;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v41;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v42;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v43;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v43;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v44;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v45;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v45;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v46;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v47;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v47;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v48;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v49;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v49;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v50;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v51;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v51;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v52;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v53;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v53;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v54;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v55;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v55;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v56;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v57;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v57;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v58;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v59;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v59;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v60;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v61;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v61;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v62;
    CData/*4:0*/ __Vdlyvdim0__circular_fifo__DOT__registers__v63;
    CData/*0:0*/ __Vdlyvset__circular_fifo__DOT__registers__v63;
    CData/*4:0*/ __Vdly__wr_ptr;
    CData/*4:0*/ __Vdly__rd_ptr;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v1;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v3;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v5;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v7;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v9;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v11;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v13;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v15;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v17;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v19;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v21;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v23;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v25;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v27;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v29;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v31;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v33;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v35;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v37;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v39;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v41;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v43;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v45;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v47;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v49;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v51;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v53;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v55;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v57;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v59;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v61;
    SData/*15:0*/ __Vdlyvval__circular_fifo__DOT__registers__v63;
    // Body
    __Vdly__wr_ptr = vlTOPp->wr_ptr;
    __Vdly__rd_ptr = vlTOPp->rd_ptr;
    vlTOPp->circular_fifo__DOT__i = 0x20U;
    __Vdlyvset__circular_fifo__DOT__registers__v0 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v1 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v2 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v3 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v4 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v5 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v6 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v7 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v8 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v9 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v10 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v11 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v12 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v13 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v14 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v15 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v16 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v17 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v18 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v19 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v20 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v21 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v22 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v23 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v24 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v25 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v26 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v27 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v28 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v29 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v30 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v31 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v32 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v33 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v34 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v35 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v36 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v37 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v38 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v39 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v40 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v41 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v42 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v43 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v44 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v45 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v46 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v47 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v48 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v49 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v50 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v51 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v52 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v53 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v54 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v55 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v56 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v57 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v58 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v59 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v60 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v61 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v62 = 0U;
    __Vdlyvset__circular_fifo__DOT__registers__v63 = 0U;
    if (vlTOPp->reset) {
        __Vdly__wr_ptr = 0U;
    } else {
        if (vlTOPp->push) {
            __Vdly__wr_ptr = (0x1fU & ((IData)(1U) 
                                       + (IData)(vlTOPp->wr_ptr)));
        }
    }
    if (vlTOPp->reset) {
        __Vdly__rd_ptr = 0U;
    } else {
        if (vlTOPp->pop) {
            __Vdly__rd_ptr = (0x1fU & ((IData)(1U) 
                                       + (IData)(vlTOPp->rd_ptr)));
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v0 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v1 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v1 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v1 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v2 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v3 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v3 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v3 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v4 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v5 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v5 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v5 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v6 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v7 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v7 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v7 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v8 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v9 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v9 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v9 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v10 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v11 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v11 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v11 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v12 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v13 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v13 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v13 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v14 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v15 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v15 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v15 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v16 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v17 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v17 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v17 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v18 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v19 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v19 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v19 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v20 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v21 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v21 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v21 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v22 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v23 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v23 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v23 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v24 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v25 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v25 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v25 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v26 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v27 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v27 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v27 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v28 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v29 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v29 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v29 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v30 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v31 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v31 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v31 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v32 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v33 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v33 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v33 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v34 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v35 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v35 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v35 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v36 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v37 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v37 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v37 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v38 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v39 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v39 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v39 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v40 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v41 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v41 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v41 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v42 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v43 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v43 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v43 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v44 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v45 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v45 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v45 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v46 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v47 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v47 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v47 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v48 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v49 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v49 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v49 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v50 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v51 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v51 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v51 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v52 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v53 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v53 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v53 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v54 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v55 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v55 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v55 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v56 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v57 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v57 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v57 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v58 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v59 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v59 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v59 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v60 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v61 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v61 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v61 
                = vlTOPp->wr_ptr;
        }
    }
    if (vlTOPp->reset) {
        __Vdlyvset__circular_fifo__DOT__registers__v62 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__circular_fifo__DOT__registers__v63 
                = vlTOPp->dataIn;
            __Vdlyvset__circular_fifo__DOT__registers__v63 = 1U;
            __Vdlyvdim0__circular_fifo__DOT__registers__v63 
                = vlTOPp->wr_ptr;
        }
    }
    vlTOPp->rd_ptr = __Vdly__rd_ptr;
    vlTOPp->wr_ptr = __Vdly__wr_ptr;
    if (__Vdlyvset__circular_fifo__DOT__registers__v0) {
        vlTOPp->circular_fifo__DOT__registers[0U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v1) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v1] 
            = __Vdlyvval__circular_fifo__DOT__registers__v1;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v2) {
        vlTOPp->circular_fifo__DOT__registers[1U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v3) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v3] 
            = __Vdlyvval__circular_fifo__DOT__registers__v3;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v4) {
        vlTOPp->circular_fifo__DOT__registers[2U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v5) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v5] 
            = __Vdlyvval__circular_fifo__DOT__registers__v5;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v6) {
        vlTOPp->circular_fifo__DOT__registers[3U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v7) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v7] 
            = __Vdlyvval__circular_fifo__DOT__registers__v7;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v8) {
        vlTOPp->circular_fifo__DOT__registers[4U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v9) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v9] 
            = __Vdlyvval__circular_fifo__DOT__registers__v9;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v10) {
        vlTOPp->circular_fifo__DOT__registers[5U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v11) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v11] 
            = __Vdlyvval__circular_fifo__DOT__registers__v11;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v12) {
        vlTOPp->circular_fifo__DOT__registers[6U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v13) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v13] 
            = __Vdlyvval__circular_fifo__DOT__registers__v13;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v14) {
        vlTOPp->circular_fifo__DOT__registers[7U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v15) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v15] 
            = __Vdlyvval__circular_fifo__DOT__registers__v15;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v16) {
        vlTOPp->circular_fifo__DOT__registers[8U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v17) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v17] 
            = __Vdlyvval__circular_fifo__DOT__registers__v17;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v18) {
        vlTOPp->circular_fifo__DOT__registers[9U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v19) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v19] 
            = __Vdlyvval__circular_fifo__DOT__registers__v19;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v20) {
        vlTOPp->circular_fifo__DOT__registers[0xaU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v21) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v21] 
            = __Vdlyvval__circular_fifo__DOT__registers__v21;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v22) {
        vlTOPp->circular_fifo__DOT__registers[0xbU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v23) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v23] 
            = __Vdlyvval__circular_fifo__DOT__registers__v23;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v24) {
        vlTOPp->circular_fifo__DOT__registers[0xcU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v25) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v25] 
            = __Vdlyvval__circular_fifo__DOT__registers__v25;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v26) {
        vlTOPp->circular_fifo__DOT__registers[0xdU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v27) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v27] 
            = __Vdlyvval__circular_fifo__DOT__registers__v27;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v28) {
        vlTOPp->circular_fifo__DOT__registers[0xeU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v29) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v29] 
            = __Vdlyvval__circular_fifo__DOT__registers__v29;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v30) {
        vlTOPp->circular_fifo__DOT__registers[0xfU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v31) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v31] 
            = __Vdlyvval__circular_fifo__DOT__registers__v31;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v32) {
        vlTOPp->circular_fifo__DOT__registers[0x10U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v33) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v33] 
            = __Vdlyvval__circular_fifo__DOT__registers__v33;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v34) {
        vlTOPp->circular_fifo__DOT__registers[0x11U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v35) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v35] 
            = __Vdlyvval__circular_fifo__DOT__registers__v35;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v36) {
        vlTOPp->circular_fifo__DOT__registers[0x12U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v37) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v37] 
            = __Vdlyvval__circular_fifo__DOT__registers__v37;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v38) {
        vlTOPp->circular_fifo__DOT__registers[0x13U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v39) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v39] 
            = __Vdlyvval__circular_fifo__DOT__registers__v39;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v40) {
        vlTOPp->circular_fifo__DOT__registers[0x14U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v41) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v41] 
            = __Vdlyvval__circular_fifo__DOT__registers__v41;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v42) {
        vlTOPp->circular_fifo__DOT__registers[0x15U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v43) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v43] 
            = __Vdlyvval__circular_fifo__DOT__registers__v43;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v44) {
        vlTOPp->circular_fifo__DOT__registers[0x16U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v45) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v45] 
            = __Vdlyvval__circular_fifo__DOT__registers__v45;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v46) {
        vlTOPp->circular_fifo__DOT__registers[0x17U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v47) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v47] 
            = __Vdlyvval__circular_fifo__DOT__registers__v47;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v48) {
        vlTOPp->circular_fifo__DOT__registers[0x18U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v49) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v49] 
            = __Vdlyvval__circular_fifo__DOT__registers__v49;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v50) {
        vlTOPp->circular_fifo__DOT__registers[0x19U] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v51) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v51] 
            = __Vdlyvval__circular_fifo__DOT__registers__v51;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v52) {
        vlTOPp->circular_fifo__DOT__registers[0x1aU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v53) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v53] 
            = __Vdlyvval__circular_fifo__DOT__registers__v53;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v54) {
        vlTOPp->circular_fifo__DOT__registers[0x1bU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v55) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v55] 
            = __Vdlyvval__circular_fifo__DOT__registers__v55;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v56) {
        vlTOPp->circular_fifo__DOT__registers[0x1cU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v57) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v57] 
            = __Vdlyvval__circular_fifo__DOT__registers__v57;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v58) {
        vlTOPp->circular_fifo__DOT__registers[0x1dU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v59) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v59] 
            = __Vdlyvval__circular_fifo__DOT__registers__v59;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v60) {
        vlTOPp->circular_fifo__DOT__registers[0x1eU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v61) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v61] 
            = __Vdlyvval__circular_fifo__DOT__registers__v61;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v62) {
        vlTOPp->circular_fifo__DOT__registers[0x1fU] = 0U;
    }
    if (__Vdlyvset__circular_fifo__DOT__registers__v63) {
        vlTOPp->circular_fifo__DOT__registers[__Vdlyvdim0__circular_fifo__DOT__registers__v63] 
            = __Vdlyvval__circular_fifo__DOT__registers__v63;
    }
    vlTOPp->fifoEmpty = (((1U & ((IData)(vlTOPp->wr_ptr) 
                                 >> 4U)) == (1U & ((IData)(vlTOPp->rd_ptr) 
                                                   >> 4U))) 
                         & ((0xfU & (IData)(vlTOPp->wr_ptr)) 
                            == (0xfU & (IData)(vlTOPp->rd_ptr))));
    vlTOPp->fifoFull = (((1U & ((IData)(vlTOPp->wr_ptr) 
                                >> 4U)) != (1U & ((IData)(vlTOPp->rd_ptr) 
                                                  >> 4U))) 
                        & ((0xfU & (IData)(vlTOPp->wr_ptr)) 
                           == (0xfU & (IData)(vlTOPp->rd_ptr))));
    vlTOPp->dataOutput = vlTOPp->circular_fifo__DOT__registers
        [vlTOPp->rd_ptr];
}

void Vc_fifo::_eval(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_eval\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) ^ (IData)(vlTOPp->__Vclklast__TOP__clk))) {
        vlTOPp->_sequent__TOP__1(vlSymsp);
        vlTOPp->__Vm_traceActivity[1U] = 1U;
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

VL_INLINE_OPT QData Vc_fifo::_change_request(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_change_request\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    return (vlTOPp->_change_request_1(vlSymsp));
}

VL_INLINE_OPT QData Vc_fifo::_change_request_1(Vc_fifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_change_request_1\n"); );
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

#ifdef VL_DEBUG
void Vc_fifo::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vc_fifo::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((clk & 0xfeU))) {
        Verilated::overWidthError("clk");}
    if (VL_UNLIKELY((reset & 0xfeU))) {
        Verilated::overWidthError("reset");}
    if (VL_UNLIKELY((push & 0xfeU))) {
        Verilated::overWidthError("push");}
    if (VL_UNLIKELY((pop & 0xfeU))) {
        Verilated::overWidthError("pop");}
}
#endif  // VL_DEBUG
