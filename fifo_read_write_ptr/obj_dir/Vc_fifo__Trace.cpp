// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vc_fifo__Syms.h"


void Vc_fifo::traceChgTop0(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    {
        vlTOPp->traceChgSub0(userp, tracep);
    }
}

void Vc_fifo::traceChgSub0(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    vluint32_t* const oldp = tracep->oldp(vlSymsp->__Vm_baseCode + 1);
    if (false && oldp) {}  // Prevent unused
    // Body
    {
        if (VL_UNLIKELY(vlTOPp->__Vm_traceActivity[1U])) {
            tracep->chgSData(oldp+0,(vlTOPp->circular_fifo__DOT__registers[0]),16);
            tracep->chgSData(oldp+1,(vlTOPp->circular_fifo__DOT__registers[1]),16);
            tracep->chgSData(oldp+2,(vlTOPp->circular_fifo__DOT__registers[2]),16);
            tracep->chgSData(oldp+3,(vlTOPp->circular_fifo__DOT__registers[3]),16);
            tracep->chgSData(oldp+4,(vlTOPp->circular_fifo__DOT__registers[4]),16);
            tracep->chgSData(oldp+5,(vlTOPp->circular_fifo__DOT__registers[5]),16);
            tracep->chgSData(oldp+6,(vlTOPp->circular_fifo__DOT__registers[6]),16);
            tracep->chgSData(oldp+7,(vlTOPp->circular_fifo__DOT__registers[7]),16);
            tracep->chgSData(oldp+8,(vlTOPp->circular_fifo__DOT__registers[8]),16);
            tracep->chgSData(oldp+9,(vlTOPp->circular_fifo__DOT__registers[9]),16);
            tracep->chgSData(oldp+10,(vlTOPp->circular_fifo__DOT__registers[10]),16);
            tracep->chgSData(oldp+11,(vlTOPp->circular_fifo__DOT__registers[11]),16);
            tracep->chgSData(oldp+12,(vlTOPp->circular_fifo__DOT__registers[12]),16);
            tracep->chgSData(oldp+13,(vlTOPp->circular_fifo__DOT__registers[13]),16);
            tracep->chgSData(oldp+14,(vlTOPp->circular_fifo__DOT__registers[14]),16);
            tracep->chgSData(oldp+15,(vlTOPp->circular_fifo__DOT__registers[15]),16);
            tracep->chgSData(oldp+16,(vlTOPp->circular_fifo__DOT__registers[16]),16);
            tracep->chgSData(oldp+17,(vlTOPp->circular_fifo__DOT__registers[17]),16);
            tracep->chgSData(oldp+18,(vlTOPp->circular_fifo__DOT__registers[18]),16);
            tracep->chgSData(oldp+19,(vlTOPp->circular_fifo__DOT__registers[19]),16);
            tracep->chgSData(oldp+20,(vlTOPp->circular_fifo__DOT__registers[20]),16);
            tracep->chgSData(oldp+21,(vlTOPp->circular_fifo__DOT__registers[21]),16);
            tracep->chgSData(oldp+22,(vlTOPp->circular_fifo__DOT__registers[22]),16);
            tracep->chgSData(oldp+23,(vlTOPp->circular_fifo__DOT__registers[23]),16);
            tracep->chgSData(oldp+24,(vlTOPp->circular_fifo__DOT__registers[24]),16);
            tracep->chgSData(oldp+25,(vlTOPp->circular_fifo__DOT__registers[25]),16);
            tracep->chgSData(oldp+26,(vlTOPp->circular_fifo__DOT__registers[26]),16);
            tracep->chgSData(oldp+27,(vlTOPp->circular_fifo__DOT__registers[27]),16);
            tracep->chgSData(oldp+28,(vlTOPp->circular_fifo__DOT__registers[28]),16);
            tracep->chgSData(oldp+29,(vlTOPp->circular_fifo__DOT__registers[29]),16);
            tracep->chgSData(oldp+30,(vlTOPp->circular_fifo__DOT__registers[30]),16);
            tracep->chgSData(oldp+31,(vlTOPp->circular_fifo__DOT__registers[31]),16);
            tracep->chgIData(oldp+32,(vlTOPp->circular_fifo__DOT__i),32);
        }
        tracep->chgBit(oldp+33,(vlTOPp->clk));
        tracep->chgBit(oldp+34,(vlTOPp->reset));
        tracep->chgBit(oldp+35,(vlTOPp->push));
        tracep->chgBit(oldp+36,(vlTOPp->pop));
        tracep->chgSData(oldp+37,(vlTOPp->dataIn),16);
        tracep->chgCData(oldp+38,(vlTOPp->wr_ptr),5);
        tracep->chgCData(oldp+39,(vlTOPp->rd_ptr),5);
        tracep->chgSData(oldp+40,(vlTOPp->dataOutput),16);
        tracep->chgBit(oldp+41,(vlTOPp->fifoFull));
        tracep->chgBit(oldp+42,(vlTOPp->fifoEmpty));
    }
}

void Vc_fifo::traceCleanup(void* userp, VerilatedVcd* /*unused*/) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlSymsp->__Vm_activity = false;
        vlTOPp->__Vm_traceActivity[0U] = 0U;
        vlTOPp->__Vm_traceActivity[1U] = 0U;
    }
}
