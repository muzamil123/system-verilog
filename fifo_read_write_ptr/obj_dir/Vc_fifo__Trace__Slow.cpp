// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vc_fifo__Syms.h"


//======================

void Vc_fifo::trace(VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addInitCb(&traceInit, __VlSymsp);
    traceRegister(tfp->spTrace());
}

void Vc_fifo::traceInit(void* userp, VerilatedVcd* tracep, uint32_t code) {
    // Callback from tracep->open()
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    if (!Verilated::calcUnusedSigs()) {
        VL_FATAL_MT(__FILE__, __LINE__, __FILE__,
                        "Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    }
    vlSymsp->__Vm_baseCode = code;
    tracep->module(vlSymsp->name());
    tracep->scopeEscape(' ');
    Vc_fifo::traceInitTop(vlSymsp, tracep);
    tracep->scopeEscape('.');
}

//======================


void Vc_fifo::traceInitTop(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlTOPp->traceInitSub0(userp, tracep);
    }
}

void Vc_fifo::traceInitSub0(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    const int c = vlSymsp->__Vm_baseCode;
    if (false && tracep && c) {}  // Prevent unused
    // Body
    {
        tracep->declBit(c+34,"clk", false,-1);
        tracep->declBit(c+35,"reset", false,-1);
        tracep->declBit(c+36,"push", false,-1);
        tracep->declBit(c+37,"pop", false,-1);
        tracep->declBus(c+38,"dataIn", false,-1, 15,0);
        tracep->declBus(c+39,"wr_ptr", false,-1, 4,0);
        tracep->declBus(c+40,"rd_ptr", false,-1, 4,0);
        tracep->declBus(c+41,"dataOutput", false,-1, 15,0);
        tracep->declBit(c+42,"fifoFull", false,-1);
        tracep->declBit(c+43,"fifoEmpty", false,-1);
        tracep->declBus(c+44,"circular_fifo DATA_WIDTH", false,-1, 31,0);
        tracep->declBus(c+45,"circular_fifo DEPTH", false,-1, 31,0);
        tracep->declBus(c+46,"circular_fifo COUNT_WIDTH", false,-1, 31,0);
        tracep->declBit(c+34,"circular_fifo clk", false,-1);
        tracep->declBit(c+35,"circular_fifo reset", false,-1);
        tracep->declBit(c+36,"circular_fifo push", false,-1);
        tracep->declBit(c+37,"circular_fifo pop", false,-1);
        tracep->declBus(c+38,"circular_fifo dataIn", false,-1, 15,0);
        tracep->declBus(c+39,"circular_fifo wr_ptr", false,-1, 4,0);
        tracep->declBus(c+40,"circular_fifo rd_ptr", false,-1, 4,0);
        tracep->declBus(c+41,"circular_fifo dataOutput", false,-1, 15,0);
        tracep->declBit(c+42,"circular_fifo fifoFull", false,-1);
        tracep->declBit(c+43,"circular_fifo fifoEmpty", false,-1);
        {int i; for (i=0; i<32; i++) {
                tracep->declBus(c+1+i*1,"circular_fifo registers", true,(i+0), 15,0);}}
        {int i; for (i=0; i<32; i++) {
                tracep->declBit(c+47+i*1,"circular_fifo dmuxRegisters", true,(i+0));}}
        tracep->declBus(c+33,"circular_fifo i", false,-1, 31,0);
        tracep->declBus(c+45,"circular_fifo counter_inst DEPTH", false,-1, 31,0);
        tracep->declBus(c+46,"circular_fifo counter_inst COUNT_WIDTH", false,-1, 31,0);
        tracep->declBit(c+34,"circular_fifo counter_inst clk", false,-1);
        tracep->declBit(c+35,"circular_fifo counter_inst reset", false,-1);
        tracep->declBit(c+36,"circular_fifo counter_inst increment", false,-1);
        tracep->declBus(c+39,"circular_fifo counter_inst count", false,-1, 4,0);
        tracep->declBus(c+45,"circular_fifo counter_inst1 DEPTH", false,-1, 31,0);
        tracep->declBus(c+46,"circular_fifo counter_inst1 COUNT_WIDTH", false,-1, 31,0);
        tracep->declBit(c+34,"circular_fifo counter_inst1 clk", false,-1);
        tracep->declBit(c+35,"circular_fifo counter_inst1 reset", false,-1);
        tracep->declBit(c+37,"circular_fifo counter_inst1 increment", false,-1);
        tracep->declBus(c+40,"circular_fifo counter_inst1 count", false,-1, 4,0);
    }
}

void Vc_fifo::traceRegister(VerilatedVcd* tracep) {
    // Body
    {
        tracep->addFullCb(&traceFullTop0, __VlSymsp);
        tracep->addChgCb(&traceChgTop0, __VlSymsp);
        tracep->addCleanupCb(&traceCleanup, __VlSymsp);
    }
}

void Vc_fifo::traceFullTop0(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlTOPp->traceFullSub0(userp, tracep);
    }
}

void Vc_fifo::traceFullSub0(void* userp, VerilatedVcd* tracep) {
    Vc_fifo__Syms* __restrict vlSymsp = static_cast<Vc_fifo__Syms*>(userp);
    Vc_fifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    vluint32_t* const oldp = tracep->oldp(vlSymsp->__Vm_baseCode);
    if (false && oldp) {}  // Prevent unused
    // Body
    {
        tracep->fullSData(oldp+1,(vlTOPp->circular_fifo__DOT__registers[0]),16);
        tracep->fullSData(oldp+2,(vlTOPp->circular_fifo__DOT__registers[1]),16);
        tracep->fullSData(oldp+3,(vlTOPp->circular_fifo__DOT__registers[2]),16);
        tracep->fullSData(oldp+4,(vlTOPp->circular_fifo__DOT__registers[3]),16);
        tracep->fullSData(oldp+5,(vlTOPp->circular_fifo__DOT__registers[4]),16);
        tracep->fullSData(oldp+6,(vlTOPp->circular_fifo__DOT__registers[5]),16);
        tracep->fullSData(oldp+7,(vlTOPp->circular_fifo__DOT__registers[6]),16);
        tracep->fullSData(oldp+8,(vlTOPp->circular_fifo__DOT__registers[7]),16);
        tracep->fullSData(oldp+9,(vlTOPp->circular_fifo__DOT__registers[8]),16);
        tracep->fullSData(oldp+10,(vlTOPp->circular_fifo__DOT__registers[9]),16);
        tracep->fullSData(oldp+11,(vlTOPp->circular_fifo__DOT__registers[10]),16);
        tracep->fullSData(oldp+12,(vlTOPp->circular_fifo__DOT__registers[11]),16);
        tracep->fullSData(oldp+13,(vlTOPp->circular_fifo__DOT__registers[12]),16);
        tracep->fullSData(oldp+14,(vlTOPp->circular_fifo__DOT__registers[13]),16);
        tracep->fullSData(oldp+15,(vlTOPp->circular_fifo__DOT__registers[14]),16);
        tracep->fullSData(oldp+16,(vlTOPp->circular_fifo__DOT__registers[15]),16);
        tracep->fullSData(oldp+17,(vlTOPp->circular_fifo__DOT__registers[16]),16);
        tracep->fullSData(oldp+18,(vlTOPp->circular_fifo__DOT__registers[17]),16);
        tracep->fullSData(oldp+19,(vlTOPp->circular_fifo__DOT__registers[18]),16);
        tracep->fullSData(oldp+20,(vlTOPp->circular_fifo__DOT__registers[19]),16);
        tracep->fullSData(oldp+21,(vlTOPp->circular_fifo__DOT__registers[20]),16);
        tracep->fullSData(oldp+22,(vlTOPp->circular_fifo__DOT__registers[21]),16);
        tracep->fullSData(oldp+23,(vlTOPp->circular_fifo__DOT__registers[22]),16);
        tracep->fullSData(oldp+24,(vlTOPp->circular_fifo__DOT__registers[23]),16);
        tracep->fullSData(oldp+25,(vlTOPp->circular_fifo__DOT__registers[24]),16);
        tracep->fullSData(oldp+26,(vlTOPp->circular_fifo__DOT__registers[25]),16);
        tracep->fullSData(oldp+27,(vlTOPp->circular_fifo__DOT__registers[26]),16);
        tracep->fullSData(oldp+28,(vlTOPp->circular_fifo__DOT__registers[27]),16);
        tracep->fullSData(oldp+29,(vlTOPp->circular_fifo__DOT__registers[28]),16);
        tracep->fullSData(oldp+30,(vlTOPp->circular_fifo__DOT__registers[29]),16);
        tracep->fullSData(oldp+31,(vlTOPp->circular_fifo__DOT__registers[30]),16);
        tracep->fullSData(oldp+32,(vlTOPp->circular_fifo__DOT__registers[31]),16);
        tracep->fullIData(oldp+33,(vlTOPp->circular_fifo__DOT__i),32);
        tracep->fullBit(oldp+34,(vlTOPp->clk));
        tracep->fullBit(oldp+35,(vlTOPp->reset));
        tracep->fullBit(oldp+36,(vlTOPp->push));
        tracep->fullBit(oldp+37,(vlTOPp->pop));
        tracep->fullSData(oldp+38,(vlTOPp->dataIn),16);
        tracep->fullCData(oldp+39,(vlTOPp->wr_ptr),5);
        tracep->fullCData(oldp+40,(vlTOPp->rd_ptr),5);
        tracep->fullSData(oldp+41,(vlTOPp->dataOutput),16);
        tracep->fullBit(oldp+42,(vlTOPp->fifoFull));
        tracep->fullBit(oldp+43,(vlTOPp->fifoEmpty));
        tracep->fullIData(oldp+44,(0x10U),32);
        tracep->fullIData(oldp+45,(0x20U),32);
        tracep->fullIData(oldp+46,(5U),32);
        tracep->fullBit(oldp+47,(vlTOPp->circular_fifo__DOT__dmuxRegisters[0]));
        tracep->fullBit(oldp+48,(vlTOPp->circular_fifo__DOT__dmuxRegisters[1]));
        tracep->fullBit(oldp+49,(vlTOPp->circular_fifo__DOT__dmuxRegisters[2]));
        tracep->fullBit(oldp+50,(vlTOPp->circular_fifo__DOT__dmuxRegisters[3]));
        tracep->fullBit(oldp+51,(vlTOPp->circular_fifo__DOT__dmuxRegisters[4]));
        tracep->fullBit(oldp+52,(vlTOPp->circular_fifo__DOT__dmuxRegisters[5]));
        tracep->fullBit(oldp+53,(vlTOPp->circular_fifo__DOT__dmuxRegisters[6]));
        tracep->fullBit(oldp+54,(vlTOPp->circular_fifo__DOT__dmuxRegisters[7]));
        tracep->fullBit(oldp+55,(vlTOPp->circular_fifo__DOT__dmuxRegisters[8]));
        tracep->fullBit(oldp+56,(vlTOPp->circular_fifo__DOT__dmuxRegisters[9]));
        tracep->fullBit(oldp+57,(vlTOPp->circular_fifo__DOT__dmuxRegisters[10]));
        tracep->fullBit(oldp+58,(vlTOPp->circular_fifo__DOT__dmuxRegisters[11]));
        tracep->fullBit(oldp+59,(vlTOPp->circular_fifo__DOT__dmuxRegisters[12]));
        tracep->fullBit(oldp+60,(vlTOPp->circular_fifo__DOT__dmuxRegisters[13]));
        tracep->fullBit(oldp+61,(vlTOPp->circular_fifo__DOT__dmuxRegisters[14]));
        tracep->fullBit(oldp+62,(vlTOPp->circular_fifo__DOT__dmuxRegisters[15]));
        tracep->fullBit(oldp+63,(vlTOPp->circular_fifo__DOT__dmuxRegisters[16]));
        tracep->fullBit(oldp+64,(vlTOPp->circular_fifo__DOT__dmuxRegisters[17]));
        tracep->fullBit(oldp+65,(vlTOPp->circular_fifo__DOT__dmuxRegisters[18]));
        tracep->fullBit(oldp+66,(vlTOPp->circular_fifo__DOT__dmuxRegisters[19]));
        tracep->fullBit(oldp+67,(vlTOPp->circular_fifo__DOT__dmuxRegisters[20]));
        tracep->fullBit(oldp+68,(vlTOPp->circular_fifo__DOT__dmuxRegisters[21]));
        tracep->fullBit(oldp+69,(vlTOPp->circular_fifo__DOT__dmuxRegisters[22]));
        tracep->fullBit(oldp+70,(vlTOPp->circular_fifo__DOT__dmuxRegisters[23]));
        tracep->fullBit(oldp+71,(vlTOPp->circular_fifo__DOT__dmuxRegisters[24]));
        tracep->fullBit(oldp+72,(vlTOPp->circular_fifo__DOT__dmuxRegisters[25]));
        tracep->fullBit(oldp+73,(vlTOPp->circular_fifo__DOT__dmuxRegisters[26]));
        tracep->fullBit(oldp+74,(vlTOPp->circular_fifo__DOT__dmuxRegisters[27]));
        tracep->fullBit(oldp+75,(vlTOPp->circular_fifo__DOT__dmuxRegisters[28]));
        tracep->fullBit(oldp+76,(vlTOPp->circular_fifo__DOT__dmuxRegisters[29]));
        tracep->fullBit(oldp+77,(vlTOPp->circular_fifo__DOT__dmuxRegisters[30]));
        tracep->fullBit(oldp+78,(vlTOPp->circular_fifo__DOT__dmuxRegisters[31]));
    }
}
