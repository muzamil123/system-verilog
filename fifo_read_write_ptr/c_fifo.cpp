#include <verilated.h>
#include "Vc_fifo.h"
#include <verilated_vcd_c.h>
#define DEPTH 32
int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    
    Vc_fifo* top_fifo = new Vc_fifo;
    top_fifo->clk = 0;
    top_fifo->reset = 1;
    top_fifo->push = 0;
    top_fifo->pop = 0;
    top_fifo->dataIn = 0;
    top_fifo->wr_ptr = 0;
    top_fifo->rd_ptr = 0;

  
    Verilated::traceEverOn(true);
    VerilatedVcdC* tfp_fifo = new VerilatedVcdC;
    top_fifo->trace(tfp_fifo, 99); 
    tfp_fifo->open("fifo_wave.vcd");
	top_fifo->reset = 0;
  
    for (int cycle = 0; cycle < 1000; ++cycle) {
       
        top_fifo->clk = !top_fifo->clk;
        top_fifo->eval();
        tfp_fifo->dump(10 * cycle + 5);

        
        if (cycle >= 5) {
            
            if (cycle % 10 == 0 && cycle < 1990) {
                top_fifo->push = 1;
                top_fifo->dataIn = cycle + 1; 
              
            } else {
                top_fifo->push = 0;
            }

           
            if (cycle >= 420 && cycle % 15 == 0) {
                top_fifo->pop = 1;
                
            } else {
                top_fifo->pop = 0;
            }
        }

     
        tfp_fifo->dump(10 * cycle + 10);
    }

   
    tfp_fifo->close();

  
    delete top_fifo;
    delete tfp_fifo;

    return 0;
}

