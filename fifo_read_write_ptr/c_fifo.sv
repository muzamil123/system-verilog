module circular_fifo
#(
parameter DATA_WIDTH = 16,
parameter DEPTH = 32,
parameter COUNT_WIDTH = $clog2(DEPTH)
)

(
input logic clk,
input logic reset,
input logic push,
input logic pop,
input logic [DATA_WIDTH-1:0] dataIn,
output logic [COUNT_WIDTH-1:0] wr_ptr,
output logic [COUNT_WIDTH-1:0]   rd_ptr,

output logic [DATA_WIDTH-1:0] dataOutput,
output  logic  fifoFull,
output   logic fifoEmpty
);

logic [DATA_WIDTH-1:0] registers [0:DEPTH-1];
logic dmuxRegisters [0:DEPTH-1] ;
integer i;
counter counter_inst
(
.clk (clk),
.reset (reset),
.increment (push),
.count (wr_ptr)

);
counter counter_inst1
(
.clk (clk),
.reset (reset),
.increment (pop),
.count (rd_ptr)

);


always_ff @ (posedge clk or negedge clk)
begin
for (i = 0; i < DEPTH; i = i + 1)
begin
if (reset)
registers[i] <= 0;
else if (push)
registers[wr_ptr]<= dataIn;
end
end


assign fifoEmpty =  (wr_ptr[COUNT_WIDTH-1] == rd_ptr[COUNT_WIDTH-1])& (wr_ptr[COUNT_WIDTH-2:0] == rd_ptr[COUNT_WIDTH-2:0]);
assign fifoFull = ~(wr_ptr[COUNT_WIDTH-1] == rd_ptr[COUNT_WIDTH-1])& (wr_ptr[COUNT_WIDTH-2:0] == rd_ptr[COUNT_WIDTH-2:0]);
assign dataOutput = registers[rd_ptr];

endmodule
