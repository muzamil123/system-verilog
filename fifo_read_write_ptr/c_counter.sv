module counter
#(
parameter DEPTH = 32,
parameter COUNT_WIDTH = $clog2(DEPTH)
)
(
input logic  clk,
input logic reset,
input logic increment,
output logic  [COUNT_WIDTH-1:0] count
);


always_ff @(posedge clk or negedge clk)
begin
if(reset)
count <=  0;
else if(increment)
count <=  count+1;
end

endmodule
