module counter #(
    parameter DEPTH = 4,
    parameter COUNT_WIDTH = $clog2(DEPTH)
)(
    input logic clk,
    input logic reset,
    input logic increment,
    input logic decrement,
    output logic [COUNT_WIDTH-1:0] count
);

    logic enable;
    logic [COUNT_WIDTH-1:0] mux_out;

    always_comb begin
        enable = increment ^ decrement;
    end

    always_comb begin
        case (increment)
            1'b0: mux_out = count - 1;
            1'b1: mux_out = count + 1;
        endcase
    end

    always_ff @(posedge clk or negedge reset) begin
        if (reset) begin
            count <= 0;
        end else if (enable) begin
            count <= mux_out;
        end
    end

endmodule

