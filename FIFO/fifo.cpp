#include <verilated.h>
#include "Vfifo.h" 
#include <verilated_vcd_c.h> 

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    Vfifo* top = new Vfifo;
    top->clk = 0;
    top->reset = 1;
    top->push = 0;
    top->pop = 0;
    top->dataIn = 0;

    Verilated::traceEverOn(true);
    VerilatedVcdC* tfp = new VerilatedVcdC;
    top->trace(tfp, 99); 
    tfp->open("fifo_wave.vcd");
	top->reset=0;


    for (int i = 0; i < 500; ++i) {

        top->clk = !top->clk;

        top->eval();

        tfp->dump(10 * i + 5);

        if (top->clk) {
            if (i % 10 == 0 && i < 490) { 
                top->push = 1;
                top->dataIn = i + 1; 
            } else {
                top->push = 0;
            }
        }


        if (top->clk && i >= 130) { 
            if (i % 15 == 0) { 
                top->pop = 1;
            } else {
                top->pop = 0;
            }
        }
    }

   


    tfp->close();

    delete top;
    delete tfp;

    return 0;
}

