// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vfifo.h for the primary calling header

#include "Vfifo.h"
#include "Vfifo__Syms.h"

//==========

VL_CTOR_IMP(Vfifo) {
    Vfifo__Syms* __restrict vlSymsp = __VlSymsp = new Vfifo__Syms(this, name());
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vfifo::__Vconfigure(Vfifo__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
    Verilated::timeunit(-12);
    Verilated::timeprecision(-12);
}

Vfifo::~Vfifo() {
    VL_DO_CLEAR(delete __VlSymsp, __VlSymsp = NULL);
}

void Vfifo::_settle__TOP__3(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_settle__TOP__3\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->fifoEmpty = (0U == (IData)(vlTOPp->fifo__DOT__count));
    vlTOPp->fifoFull = (3U == (IData)(vlTOPp->fifo__DOT__count));
    vlTOPp->dataOutput = vlTOPp->fifo__DOT__registers
        [vlTOPp->fifo__DOT__count];
    if (vlTOPp->push) {
        if (vlTOPp->push) {
            vlTOPp->fifo__DOT__counter_inst__DOT__mux_out 
                = (3U & ((IData)(1U) + (IData)(vlTOPp->fifo__DOT__count)));
        }
    } else {
        vlTOPp->fifo__DOT__counter_inst__DOT__mux_out 
            = (3U & ((IData)(vlTOPp->fifo__DOT__count) 
                     - (IData)(1U)));
    }
}

void Vfifo::_eval_initial(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_eval_initial\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
    vlTOPp->__Vclklast__TOP__reset = vlTOPp->reset;
}

void Vfifo::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::final\n"); );
    // Variables
    Vfifo__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vfifo::_eval_settle(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_eval_settle\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__3(vlSymsp);
}

void Vfifo::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_ctor_var_reset\n"); );
    // Body
    clk = VL_RAND_RESET_I(1);
    reset = VL_RAND_RESET_I(1);
    dataIn = VL_RAND_RESET_I(16);
    push = VL_RAND_RESET_I(1);
    pop = VL_RAND_RESET_I(1);
    fifoFull = VL_RAND_RESET_I(1);
    fifoEmpty = VL_RAND_RESET_I(1);
    dataOutput = VL_RAND_RESET_I(16);
    fifo__DOT__count = VL_RAND_RESET_I(2);
    { int __Vi0=0; for (; __Vi0<4; ++__Vi0) {
            fifo__DOT__registers[__Vi0] = VL_RAND_RESET_I(16);
    }}
    fifo__DOT__unnamedblk1__DOT__i = 0;
    fifo__DOT__unnamedblk2__DOT__i = 0;
    fifo__DOT__counter_inst__DOT__mux_out = VL_RAND_RESET_I(2);
    { int __Vi0=0; for (; __Vi0<2; ++__Vi0) {
            __Vm_traceActivity[__Vi0] = VL_RAND_RESET_I(1);
    }}
}
