// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vfifo.h for the primary calling header

#include "Vfifo.h"
#include "Vfifo__Syms.h"

//==========

void Vfifo::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vfifo::eval\n"); );
    Vfifo__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        vlSymsp->__Vm_activity = true;
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("fifo.sv", 1, "",
                "Verilated model didn't converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Vfifo::_eval_initial_loop(Vfifo__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        _eval_settle(vlSymsp);
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("fifo.sv", 1, "",
                "Verilated model didn't DC converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

VL_INLINE_OPT void Vfifo::_sequent__TOP__1(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_sequent__TOP__1\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->reset) {
        vlTOPp->fifo__DOT__count = 0U;
    } else {
        if (((IData)(vlTOPp->push) ^ (IData)(vlTOPp->pop))) {
            vlTOPp->fifo__DOT__count = vlTOPp->fifo__DOT__counter_inst__DOT__mux_out;
        }
    }
    vlTOPp->fifoEmpty = (0U == (IData)(vlTOPp->fifo__DOT__count));
    vlTOPp->fifoFull = (3U == (IData)(vlTOPp->fifo__DOT__count));
}

VL_INLINE_OPT void Vfifo::_sequent__TOP__2(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_sequent__TOP__2\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    CData/*0:0*/ __Vdlyvset__fifo__DOT__registers__v0;
    CData/*0:0*/ __Vdlyvset__fifo__DOT__registers__v4;
    SData/*15:0*/ __Vdlyvval__fifo__DOT__registers__v4;
    SData/*15:0*/ __Vdlyvval__fifo__DOT__registers__v5;
    SData/*15:0*/ __Vdlyvval__fifo__DOT__registers__v6;
    SData/*15:0*/ __Vdlyvval__fifo__DOT__registers__v7;
    // Body
    if (vlTOPp->reset) {
        vlTOPp->fifo__DOT__unnamedblk1__DOT__i = 4U;
    }
    if ((1U & (~ (IData)(vlTOPp->reset)))) {
        if (vlTOPp->push) {
            vlTOPp->fifo__DOT__unnamedblk2__DOT__i = 4U;
        }
    }
    __Vdlyvset__fifo__DOT__registers__v0 = 0U;
    __Vdlyvset__fifo__DOT__registers__v4 = 0U;
    if (vlTOPp->reset) {
        __Vdlyvset__fifo__DOT__registers__v0 = 1U;
    } else {
        if (vlTOPp->push) {
            __Vdlyvval__fifo__DOT__registers__v4 = vlTOPp->dataIn;
            __Vdlyvset__fifo__DOT__registers__v4 = 1U;
            __Vdlyvval__fifo__DOT__registers__v5 = 
                vlTOPp->fifo__DOT__registers[0U];
            __Vdlyvval__fifo__DOT__registers__v6 = 
                vlTOPp->fifo__DOT__registers[1U];
            __Vdlyvval__fifo__DOT__registers__v7 = 
                vlTOPp->fifo__DOT__registers[2U];
        }
    }
    if (__Vdlyvset__fifo__DOT__registers__v0) {
        vlTOPp->fifo__DOT__registers[0U] = 0U;
        vlTOPp->fifo__DOT__registers[1U] = 0U;
        vlTOPp->fifo__DOT__registers[2U] = 0U;
        vlTOPp->fifo__DOT__registers[3U] = 0U;
    }
    if (__Vdlyvset__fifo__DOT__registers__v4) {
        vlTOPp->fifo__DOT__registers[0U] = __Vdlyvval__fifo__DOT__registers__v4;
        vlTOPp->fifo__DOT__registers[1U] = __Vdlyvval__fifo__DOT__registers__v5;
        vlTOPp->fifo__DOT__registers[2U] = __Vdlyvval__fifo__DOT__registers__v6;
        vlTOPp->fifo__DOT__registers[3U] = __Vdlyvval__fifo__DOT__registers__v7;
    }
}

VL_INLINE_OPT void Vfifo::_combo__TOP__4(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_combo__TOP__4\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->push) {
        if (vlTOPp->push) {
            vlTOPp->fifo__DOT__counter_inst__DOT__mux_out 
                = (3U & ((IData)(1U) + (IData)(vlTOPp->fifo__DOT__count)));
        }
    } else {
        vlTOPp->fifo__DOT__counter_inst__DOT__mux_out 
            = (3U & ((IData)(vlTOPp->fifo__DOT__count) 
                     - (IData)(1U)));
    }
}

VL_INLINE_OPT void Vfifo::_sequent__TOP__5(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_sequent__TOP__5\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->dataOutput = vlTOPp->fifo__DOT__registers
        [vlTOPp->fifo__DOT__count];
}

void Vfifo::_eval(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_eval\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if ((((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk))) 
         | ((~ (IData)(vlTOPp->reset)) & (IData)(vlTOPp->__Vclklast__TOP__reset)))) {
        vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
        vlTOPp->_sequent__TOP__2(vlSymsp);
        vlTOPp->__Vm_traceActivity[1U] = 1U;
    }
    vlTOPp->_combo__TOP__4(vlSymsp);
    if ((((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk))) 
         | ((~ (IData)(vlTOPp->reset)) & (IData)(vlTOPp->__Vclklast__TOP__reset)))) {
        vlTOPp->_sequent__TOP__5(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
    vlTOPp->__Vclklast__TOP__reset = vlTOPp->reset;
}

VL_INLINE_OPT QData Vfifo::_change_request(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_change_request\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    return (vlTOPp->_change_request_1(vlSymsp));
}

VL_INLINE_OPT QData Vfifo::_change_request_1(Vfifo__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_change_request_1\n"); );
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

#ifdef VL_DEBUG
void Vfifo::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vfifo::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((clk & 0xfeU))) {
        Verilated::overWidthError("clk");}
    if (VL_UNLIKELY((reset & 0xfeU))) {
        Verilated::overWidthError("reset");}
    if (VL_UNLIKELY((push & 0xfeU))) {
        Verilated::overWidthError("push");}
    if (VL_UNLIKELY((pop & 0xfeU))) {
        Verilated::overWidthError("pop");}
}
#endif  // VL_DEBUG
