// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vfifo__Syms.h"


//======================

void Vfifo::trace(VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addInitCb(&traceInit, __VlSymsp);
    traceRegister(tfp->spTrace());
}

void Vfifo::traceInit(void* userp, VerilatedVcd* tracep, uint32_t code) {
    // Callback from tracep->open()
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    if (!Verilated::calcUnusedSigs()) {
        VL_FATAL_MT(__FILE__, __LINE__, __FILE__,
                        "Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    }
    vlSymsp->__Vm_baseCode = code;
    tracep->module(vlSymsp->name());
    tracep->scopeEscape(' ');
    Vfifo::traceInitTop(vlSymsp, tracep);
    tracep->scopeEscape('.');
}

//======================


void Vfifo::traceInitTop(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlTOPp->traceInitSub0(userp, tracep);
    }
}

void Vfifo::traceInitSub0(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    const int c = vlSymsp->__Vm_baseCode;
    if (false && tracep && c) {}  // Prevent unused
    // Body
    {
        tracep->declBit(c+7,"clk", false,-1);
        tracep->declBit(c+8,"reset", false,-1);
        tracep->declBus(c+9,"dataIn", false,-1, 15,0);
        tracep->declBit(c+10,"push", false,-1);
        tracep->declBit(c+11,"pop", false,-1);
        tracep->declBit(c+12,"fifoFull", false,-1);
        tracep->declBit(c+13,"fifoEmpty", false,-1);
        tracep->declBus(c+14,"dataOutput", false,-1, 15,0);
        tracep->declBus(c+18,"fifo DATA_WIDTH", false,-1, 31,0);
        tracep->declBus(c+19,"fifo DEPTH", false,-1, 31,0);
        tracep->declBus(c+20,"fifo COUNT_WIDTH", false,-1, 31,0);
        tracep->declBit(c+7,"fifo clk", false,-1);
        tracep->declBit(c+8,"fifo reset", false,-1);
        tracep->declBus(c+9,"fifo dataIn", false,-1, 15,0);
        tracep->declBit(c+10,"fifo push", false,-1);
        tracep->declBit(c+11,"fifo pop", false,-1);
        tracep->declBit(c+12,"fifo fifoFull", false,-1);
        tracep->declBit(c+13,"fifo fifoEmpty", false,-1);
        tracep->declBus(c+14,"fifo dataOutput", false,-1, 15,0);
        tracep->declBus(c+15,"fifo count", false,-1, 1,0);
        {int i; for (i=0; i<4; i++) {
                tracep->declBus(c+1+i*1,"fifo registers", true,(i+0), 15,0);}}
        tracep->declBus(c+5,"fifo unnamedblk1 i", false,-1, 31,0);
        tracep->declBus(c+6,"fifo unnamedblk2 i", false,-1, 31,0);
        tracep->declBus(c+19,"fifo counter_inst DEPTH", false,-1, 31,0);
        tracep->declBus(c+20,"fifo counter_inst COUNT_WIDTH", false,-1, 31,0);
        tracep->declBit(c+7,"fifo counter_inst clk", false,-1);
        tracep->declBit(c+8,"fifo counter_inst reset", false,-1);
        tracep->declBit(c+10,"fifo counter_inst increment", false,-1);
        tracep->declBit(c+11,"fifo counter_inst decrement", false,-1);
        tracep->declBus(c+15,"fifo counter_inst count", false,-1, 1,0);
        tracep->declBit(c+16,"fifo counter_inst enable", false,-1);
        tracep->declBus(c+17,"fifo counter_inst mux_out", false,-1, 1,0);
    }
}

void Vfifo::traceRegister(VerilatedVcd* tracep) {
    // Body
    {
        tracep->addFullCb(&traceFullTop0, __VlSymsp);
        tracep->addChgCb(&traceChgTop0, __VlSymsp);
        tracep->addCleanupCb(&traceCleanup, __VlSymsp);
    }
}

void Vfifo::traceFullTop0(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlTOPp->traceFullSub0(userp, tracep);
    }
}

void Vfifo::traceFullSub0(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    vluint32_t* const oldp = tracep->oldp(vlSymsp->__Vm_baseCode);
    if (false && oldp) {}  // Prevent unused
    // Body
    {
        tracep->fullSData(oldp+1,(vlTOPp->fifo__DOT__registers[0]),16);
        tracep->fullSData(oldp+2,(vlTOPp->fifo__DOT__registers[1]),16);
        tracep->fullSData(oldp+3,(vlTOPp->fifo__DOT__registers[2]),16);
        tracep->fullSData(oldp+4,(vlTOPp->fifo__DOT__registers[3]),16);
        tracep->fullIData(oldp+5,(vlTOPp->fifo__DOT__unnamedblk1__DOT__i),32);
        tracep->fullIData(oldp+6,(vlTOPp->fifo__DOT__unnamedblk2__DOT__i),32);
        tracep->fullBit(oldp+7,(vlTOPp->clk));
        tracep->fullBit(oldp+8,(vlTOPp->reset));
        tracep->fullSData(oldp+9,(vlTOPp->dataIn),16);
        tracep->fullBit(oldp+10,(vlTOPp->push));
        tracep->fullBit(oldp+11,(vlTOPp->pop));
        tracep->fullBit(oldp+12,(vlTOPp->fifoFull));
        tracep->fullBit(oldp+13,(vlTOPp->fifoEmpty));
        tracep->fullSData(oldp+14,(vlTOPp->dataOutput),16);
        tracep->fullCData(oldp+15,(vlTOPp->fifo__DOT__count),2);
        tracep->fullBit(oldp+16,(((IData)(vlTOPp->push) 
                                  ^ (IData)(vlTOPp->pop))));
        tracep->fullCData(oldp+17,(vlTOPp->fifo__DOT__counter_inst__DOT__mux_out),2);
        tracep->fullIData(oldp+18,(0x10U),32);
        tracep->fullIData(oldp+19,(4U),32);
        tracep->fullIData(oldp+20,(2U),32);
    }
}
