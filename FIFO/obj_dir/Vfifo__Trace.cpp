// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vfifo__Syms.h"


void Vfifo::traceChgTop0(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    {
        vlTOPp->traceChgSub0(userp, tracep);
    }
}

void Vfifo::traceChgSub0(void* userp, VerilatedVcd* tracep) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    vluint32_t* const oldp = tracep->oldp(vlSymsp->__Vm_baseCode + 1);
    if (false && oldp) {}  // Prevent unused
    // Body
    {
        if (VL_UNLIKELY(vlTOPp->__Vm_traceActivity[1U])) {
            tracep->chgSData(oldp+0,(vlTOPp->fifo__DOT__registers[0]),16);
            tracep->chgSData(oldp+1,(vlTOPp->fifo__DOT__registers[1]),16);
            tracep->chgSData(oldp+2,(vlTOPp->fifo__DOT__registers[2]),16);
            tracep->chgSData(oldp+3,(vlTOPp->fifo__DOT__registers[3]),16);
            tracep->chgIData(oldp+4,(vlTOPp->fifo__DOT__unnamedblk1__DOT__i),32);
            tracep->chgIData(oldp+5,(vlTOPp->fifo__DOT__unnamedblk2__DOT__i),32);
        }
        tracep->chgBit(oldp+6,(vlTOPp->clk));
        tracep->chgBit(oldp+7,(vlTOPp->reset));
        tracep->chgSData(oldp+8,(vlTOPp->dataIn),16);
        tracep->chgBit(oldp+9,(vlTOPp->push));
        tracep->chgBit(oldp+10,(vlTOPp->pop));
        tracep->chgBit(oldp+11,(vlTOPp->fifoFull));
        tracep->chgBit(oldp+12,(vlTOPp->fifoEmpty));
        tracep->chgSData(oldp+13,(vlTOPp->dataOutput),16);
        tracep->chgCData(oldp+14,(vlTOPp->fifo__DOT__count),2);
        tracep->chgBit(oldp+15,(((IData)(vlTOPp->push) 
                                 ^ (IData)(vlTOPp->pop))));
        tracep->chgCData(oldp+16,(vlTOPp->fifo__DOT__counter_inst__DOT__mux_out),2);
    }
}

void Vfifo::traceCleanup(void* userp, VerilatedVcd* /*unused*/) {
    Vfifo__Syms* __restrict vlSymsp = static_cast<Vfifo__Syms*>(userp);
    Vfifo* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    {
        vlSymsp->__Vm_activity = false;
        vlTOPp->__Vm_traceActivity[0U] = 0U;
        vlTOPp->__Vm_traceActivity[1U] = 0U;
    }
}
