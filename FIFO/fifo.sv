module fifo #(
    parameter DATA_WIDTH = 16,
    parameter DEPTH = 4,
    parameter COUNT_WIDTH = $clog2(DEPTH)
)
(
    input logic clk,
    input logic reset,
    input logic [DATA_WIDTH-1:0] dataIn,
    input logic push,
    input logic pop,
    output logic fifoFull,
    output logic fifoEmpty,
    output logic [DATA_WIDTH-1:0] dataOutput
);

    logic [COUNT_WIDTH-1:0] count;
    logic [DATA_WIDTH-1:0] registers [0:DEPTH-1];

    counter counter_inst (
        .clk(clk),
        .reset(reset),
        .increment(push),
        .decrement(pop),
        .count(count)
    );

    always_ff @(posedge clk) begin
        if (reset) begin
            for (int i = 0; i < DEPTH; i++) begin
                registers[i] <= 0;
            end
        end else if (push) begin
            registers[0] <= dataIn;
            for (int i = 1; i < DEPTH; i++) begin
                registers[i] <= registers[i-1];
            end
        end
    end

    assign dataOutput = registers[count];
    assign fifoEmpty = (count == 0);
    assign fifoFull = (count == ((1 << COUNT_WIDTH) - 1));



endmodule

